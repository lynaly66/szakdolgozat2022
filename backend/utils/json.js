const getObjectFromSqlResult = (sqlResult, keyword) => {
    var returnObject = [];
    sqlResult.forEach((result) => {
        returnObject.push(JSON.parse(result[keyword]));
    })
    console.log(returnObject)
    return returnObject;
};

const parseIngredients = (sqlResult) => {
    sqlResult.forEach((result) => {
        result.ingredients = JSON.parse(result.ingredients);
    })
    return sqlResult;
};

const parseMeals = (sqlResult) => {
    sqlResult.forEach((result) => {
        result.meals = JSON.parse(result.meals);
    })
    return sqlResult;
}


module.exports = {
    "getObjectFromSqlResult": getObjectFromSqlResult,
    "parseIngredients": parseIngredients,
    "parseMeals": parseMeals
}
