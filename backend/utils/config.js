const getConfig = (config) => {
    var env = process.env.ENVIRONMENT;
    var json = require(`../config/${env}.json`);
    return json[config];
};

module.exports = {
    "getConfig": getConfig,
};