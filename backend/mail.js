var nodemailer = require('nodemailer');
const _config = require('./utils/config');


const email_config = _config.getConfig('email');

const sendMail = (email, token, callback) => {
    console.log(`Sending mail to: ${email}`);

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: email_config.user,
          pass: email_config.pass
        }
      });

    var mailOptions = {
        from: 'forgottenpassword@recipes.com',
        to: email,
        subject: 'Reset Password Link',
        html: `To reset password use <a href="http://localhost:3000/reset-password?token=${token}">this link.</a>`
    };
 
    transporter.sendMail(mailOptions, (err, info) => {
        callback(err, info)
    });
}

module.exports = {
    'sendMail': sendMail
}
