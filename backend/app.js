var createError = require('http-errors');
var express = require('express');
var path = require('path');
var session = require('express-session');
var logger = require('morgan');
var mysql_connection = require('./connector/mysql')
var config = require('./utils/config');
var { defaultErrorHandler } = require('./errors');

var authRouter = require('./routes/auth');
var recipesRouter = require('./routes/recipes');
var collectionsRouter = require('./routes/collections');
var weekplansRouter = require('./routes/weekplans');

var sessionSecret = config.getConfig('auth').sessionSecret
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: sessionSecret
}));

app.use('/', authRouter);
//app.use(auth.restrict); todo uncomment
app.use('/recipes', recipesRouter);
app.use('/collections', collectionsRouter);
app.use('/weekplans', weekplansRouter);

app.use(defaultErrorHandler);

app.on('exit', () => {
  mysql_connection.end();
})


module.exports = app;