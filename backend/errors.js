const createError = (message, statusCode) => { 
    var x = {
        "statusCode" : statusCode || 500,
        "message": message
    }
    console.log(JSON.stringify(x))
    return x 
}

const defaultErrorHandler = (err, req, res, next) => {
    console.error(err.message);
    var env = process.env.ENVIRONMENT;
    if (env && env == "dev") console.error(err.stack)
    return res.status(err.statusCode).send(err);
}

module.exports = {
    "defaultErrorHandler": defaultErrorHandler,
    "createError": createError,
}