var express = require('express');
var router = express.Router();
const mysql_connection = require('../connector/mysql');
var createError = require('http-errors');
const { getObjectFromSqlResult, parseIngredients } = require('../utils/json');
const mysql = require('../connector/mysql');

router.get('/create', function (req, res, next) {
    res.render('create-recipe');
});



router.post('/create', async (req, res, next) => {
    try {
        var userId = req.body.userId ? req.body.userId : req.session.user.id;
        var sql = `insert into recipe (user_id, title, persons, description) values (?,?,?,?)`
        var result = mysql_connection.makeQuery(sql, [userId, req.body.title, req.body.persons, req.body.description]);
        var newRecipeId = result.insertId;
        sql = `insert into ingredient (recipe_id, amount, unit, name) values`
        req.body.ingredients.forEach(element => {
            sql += ` ('${newRecipeId}','${element["amount"]}','${element["unit"]}','${element["name"]}'),`
        });
        await mysql_connection.makeQuery(sql);
        return res.send(result.affectedRows);
    } catch (error) {
        return next(error)
    }
});

//GET all recipes of user with :userId
router.get('/user/:userId', async (req, res, next) => {
    try {
        var userId = req.params['userId'] ? req.params['userId'] : req.session.user.id;
        var sql = `select 
        json_object(
            'recipe_id', r.id,
            'title', r.title,
            'persons', r.persons,
            'ingredients', json_arrayagg(
                json_object(
                    'amount', i.amount,
                    'unit', i.unit,
                    'name', i.name
                )
            )
        ) result
        from recipe r 
        inner join ingredient i on r.id = i.recipe_id
        where r.user_id = ?
        group by r.id`
        var result = await mysql_connection.makeQuery(sql, userId);
        return res.send(getObjectFromSqlResult(result, 'result'))
    } catch (error) {
        return next(error)
    }
});

//POST add recipe to collection
router.post('/add-to-collection', async (req, res, next) => {
    try {
        var sql = `insert into recipe_collection (recipe_id, collection_id) values (?, ?)`;
        var result = await mysql_connection.makeQuery(sql, [req.body.recipeId, req.body.collectionId]);
        return res.send(result.affectedRows);
    } catch (error) {
        return next(error)
    }
})

//GET get recipes in collection :collectionId
router.get('/collection/:collectionId', async (req, res, next) => {
    try {
        var sql = `select r.id, r.title, r.persons, json_arrayagg(
                            json_object(
                                'amount', i.amount,
                                'unit', i.unit,
                                'name', i.name
                            ) 
                        ) ingredient
                    from recipe r
                    join recipe_collection rc on r.id = rc.recipe_id
                    LEFT join ingredient i on r.id = i.recipe_id
                    where rc.collection_id = ?
                    group by r.id; `;
        var result = await mysql_connection.makeQuery(sql, req.params.collectionId)
        return res.send(parseIngredients(result));
    } catch (error) {
        return next(error);
    }
});



module.exports = router;
