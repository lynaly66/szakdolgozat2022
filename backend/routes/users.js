var express = require('express');
var router = express.Router();
const mysql_connection = require('../connector/mysql');


router.get('/all', (req, res) => {
  mysql_connection.query("SELECT * FROM recipes.user", (err, rows, fields) => { 
    if (err) throw err;
    res.send(rows);
  });
});

module.exports = router;
