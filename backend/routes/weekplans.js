var express = require('express');
var router = express.Router();
const mysql_connection = require('../connector/mysql');
const { parseMeals } = require('../utils/json');

//SAVE weekplan
router.post('/', async (req, res, next) => {
    try {
        var weekplan = req.body
        console.log(`weekplan: ${JSON.stringify(weekplan)}`)
        var sql = `insert into weekplan (name, user_id) values (?, ?)`;
        var result = await mysql_connection.makeQuery(sql, [weekplan['name'], weekplan['userId']]);
        var weekplanId = result.insertId;
        console.log(`Weekplan saved successfully: ${weekplanId}`);
        var meals = weekplan['meals'];
        sql = 'insert into meal (weekplan_id, title, `index`) values (?, ?, ?)'
        for (const meal of meals) {
            result = await mysql_connection.makeQuery(sql, [weekplanId, meal['title'], meal['index']]);
            var mealId = result.insertId;
            console.log(`Meal saved successfully: ${mealId}`);
            var entries = meal['entries'];
            sql = `insert into planentry (meal_id, content, recipe_id, day_of_week) values (?, ?, ?, ?)`
            for (const entry of entries) {
                result = await mysql_connection.makeQuery(sql, [
                    mealId,
                    entry['content'],
                    entry['recipeId'],
                    entry['dayOfWeek']
                ]);
                console.log(`Entry saved successfully: ${result.insertId}`)
            }
        }
        res.send('ok');
    } catch (error) {
        return next(error)
    }
});

router.get('/user/:userId', async (req, res, next) => {
    var sql = `select
                    w.id,
                    w.user_id,
                    w.name,
                    json_arrayagg(json_object(
                        'id', base.meal_id,
                        'title', base.meal_title,
                        'index', base.meal_index,
                        'entries', base.entries
                    )) meals
                from
                (
                select
                    m.id meal_id,
                    m.weekplan_id weekplan_id,
                    m.title meal_title,
                    m.index meal_index,
                    json_arrayagg(json_object(
                        'entryId', p.id,
                        'recipeId', p.recipe_id,
                        'content', p.content,
                        'dayOfWeek', p.day_of_week
                    )) entries
                from
                    meal m
                join planentry p on
                    m.id = p.meal_id
                group by
                    1,2,3,4) base
                join weekplan w on
                w.id = base.weekplan_id
                WHERE
                user_id = 1
                group by 1, 2, 3;`
    var result = await mysql_connection.makeQuery(sql, req.params['userId']);
    res.send(parseMeals(result));
})


module.exports = router;
