var express = require('express');
var router = express.Router();
const mysql_connection = require('../connector/mysql');


//GET all for user
router.get('/user/:userId', async (req, res, next) => {
    var sql = `select * from collection where user_id = ?`;
    try {
        var result = await mysql_connection.makeQuery(sql, req.params['userId'])
        return res.send(result);
    } catch (error) {
        return next(error)
    }
})

//POST create collection
router.post('/create', async (req, res, next) => {
    var sql = `insert into collection (name, parent) values (?, ?)`;
    try {
        var result = await mysql_connection.makeQuery(sql, [req.body.name, req.body.parent]);
        return res.send(result.affectedRows);
    } catch (error) {
        return next(error)
    }
})


module.exports = router;
