var express = require('express');
const { handle } = require('express/lib/application');
var router = express.Router();
const auth = require('../auth')
const mysql_connection = require('../connector/mysql')
const randtoken = require('rand-token');
const { sendMail } = require('../mail');
const { createError } = require('../errors');
const redisClient = require('../connector/redis');



const handleUser = (user, req, res) => {
  if (user) {
    const accessToken = jwt.sign({ username: user.username }, accessTokenSecret, { expiresIn: '20m' });
    const refreshToken = jwt.sign({ username: user.username }, refreshTokenSecret);

    await redisClient.set(user.username, refreshToken);

    res.json({
      accessToken,
      refreshToken
    });
  } else {
    res.sendStatus(401)
  }
}

router.post('/login', (req, res, next) => {
  try {
    /*await*/ auth.authenticate(req.body.username, req.body.password, (user) => {
    handleUser(user, req, res);
  });
  } catch (error) {
    return next(createError(error.message));
  }
});

router.post('/register', async (req, res, next) => {
  try {
    if (req.body.password != req.body.pw_again) {
      req.session.error = `Passwords don't match.`;
      res.redirect('/register');
    }
    console.log(req.body);
    await auth.register(req.body.username, req.body.password, req.body.email, (user) => {
      handleUser(user, req, res);
    })
  } catch (error) {
    return next(error);
  }
});

router.post('/logout', (req, res) => {
  const { username} = req.body;
  await redisClient.del(username);
  res.send("Logout successful");
});

router.get('/forgotten-password', (req, res, next) => {
  res.render('forgotten-password');
});

router.post('/forgotten-password', async (req, res, next) => {
  try {
    var token = randtoken.generate(20);
    var sql = `update user set reset_token = ? where email = ?`;
    var result = await mysql_connection.makeQuery(sql, [token, req.body.email]);
    if (result.affectedRows == 0) {
      var myerr = createError(`Email ${req.body.email} not found`, 400);
      console.log(JSON.stringify(myerr))
      return next(myerr);
    }
    sendMail(req.body.email, token, (err, info) => {
      var myerr = createError(`Problems with sending email ${err.name}: ${err.message}`)
      console.log(JSON.stringify(myerr))
      if (err) return next(myerr)
      console.log(`Message sent. id: ${info.messageId}`)
      res.redirect('/login')
    });
  } catch (error) {
    return next(error);
  }
});

router.get('/reset-password', (req, res, next) => {
  res.render('reset-password', {
    'token': req.query.token
  })
});

router.post('/reset-password', async (req, res, next) => {
  try {
    var hashed_pw = auth.getSaltedPasswordHash(req.body.password);
    var sql = `update user set pw_hash='${hashed_pw}', reset_token=null where reset_token='${req.body.token}'`;
    var result = await mysql_connection.makeQuery(sql, [hashed_pw, req.body.token]);
    if (result.affectedRows == 0) {
      var myerr = createError(`Email ${req.body.token} not found`, 400);
      console.log(JSON.stringify(myerr))
      return next(myerr);
    }
    res.redirect('/login');

  } catch (error) {
    return next(error)
  }
});

router.post('/token', (req, res) => {
  const { username, token } = req.body;

  if (!token) {
    return res.sendStatus(401);
  }

  var tokenInRedis = await redisClient.get(username);

  if (!tokenInRedis) {
    return res.sendStatus(403);
  }

  jwt.verify(token, refreshTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }

    const accessToken = jwt.sign({ username: user.username }, accessTokenSecret, { expiresIn: '20m' });

    res.json({
      accessToken
    });
  });
});


module.exports = router;