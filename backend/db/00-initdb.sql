 create table user (
    id int auto_increment primary key not null,
    username varchar(100) not null,
    pw_hash varchar(100) not null,
    email varchar(100) not null,
    reset_token varchar(100),
    unique(username),
    unique(email)
);
create table collection (
    id int auto_increment primary key not null,
    name VARCHAR(100) not NULL,
    created_at datetime not null default current_timestamp,
    parent int,
    user_id int not null,
foreign key (user_id) REFERENCES user(id),
    foreign key (parent) REFERENCES collection(id)
);
create table recipe (
    id int auto_increment primary key not null,
    user_id int not null,
    title VARCHAR(100) not NULL,
    persons int not null,
    created_at datetime not null default current_timestamp,
    updated_at datetime not null default current_timestamp on update current_timestamp,
    description varchar(100),
    foreign key (user_id) REFERENCES user(id)
);
create table recipe_collection (
    id int AUTO_INCREMENT PRIMARY KEY not null,
    collection_id int not null,
    recipe_id int not null,
    foreign key (collection_id) REFERENCES collection(id),
    foreign key (recipe_id) REFERENCES recipe(id)
);
create table ingredient (
    id int auto_increment primary key not null,
    recipe_id int not null,
    amount int not null,
    unit VARCHAR(3) not NULL,
    name VARCHAR(100) not NULL,
    description varchar(100),
    foreign key (recipe_id) REFERENCES recipe(id)
);
create table weekplan (
    id int auto_increment primary key not null,
    user_id int not null,
    name VARCHAR(100) not NULL,
    created_at datetime not null default current_timestamp,
    updated_at datetime not null default current_timestamp on update current_timestamp,
    foreign key (user_id) REFERENCES user(id)
);
create table meal (
    id int auto_increment primary key not null,
    weekplan_id int not null,
    title VARCHAR(100) not NULL,
    `index` int not null,
    foreign key (weekplan_id) REFERENCES weekplan(id)
);
create table planentry (
    id int auto_increment primary key not null,
    meal_id int not null,
    recipe_id int,
    content VARCHAR(100) not NULL,
    day_of_week int not null,
    foreign key (meal_id) REFERENCES meal(id),
    foreign key (recipe_id) REFERENCES recipe(id)
);
