const _mysql = require('mysql');
const _config = require('../utils/config');


var mysql_config = _config.getConfig('mysql');
var mysql_connection = _mysql.createConnection({
    host: mysql_config.host,
    user: mysql_config.user,
    password: mysql_config.password,
    database: mysql_config.database
});
mysql_connection.connect()

const makeQuery = (sql, params) => {
    return new Promise((resolve, reject) => {
        mysql_connection.query(sql, params, (err, result) => {
            if (err) return reject(err);
            return resolve(result);
        });
    })
}
module.exports = {'makeQuery': makeQuery}