const mysql_connection = require('./connector/mysql');
const redisClient = require('./connector/redis');
var md5 = require('crypto-js/md5');
var config = require('./utils/config');

const salt = config.getConfig('auth').salt;

const getSaltedPasswordHash = (plain_password) => {
    return md5(plain_password + salt).toString();
};

const authenticate = async (username, password, callback) => {
    var sql = `select * from user where username=?`;
    var result = await mysql_connection.makeQuery(sql, username);
    if (result.length == 0) throw 'username not found';
    user = rows[0];
    var hashed_pw = getSaltedPasswordHash(password);
    if (hashed_pw == user.pw_hash) {
        callback(user);
    }
};

const register = async (username, password, email, callback) => {
    var hashed_pw = getSaltedPasswordHash(password);
    var sql = `insert into user (username, pw_hash, email) values ('${username}', '${hashed_pw}', '${email}')`;
    console.log(sql);
    var result = await mysql_connection.makeQuery(sql);
    console.log(`user ${username} registered successfully.`);
    callback({
        'username': username,
        'pw_hash': hashed_pw,
        'email': email
    });
};

const restrict = (req, res, next) => {
    if (req.session.user){
        next()
    } else { 
        req.session.error = 'Access denied!';
        res.redirect('/login');
    }
}

module.exports = {
    'authenticate': authenticate,
    'register': register,
    'restrict': restrict,
    'getSaltedPasswordHash': getSaltedPasswordHash
}